;(function () {
    var appServ = angular.module('MainApp.services',[])
        .factory('products', function($http) {
            return {
                getProducts: function() {
                    return $http.get(getUrl('api/products/'));
                },
                getComments : function(id) {
                    return $http.get(getUrl('api/reviews/'+id));
                },
                comment : function (id,data) {
                    return $http.post(getUrl('api/reviews/'+id),data);
                }
            }
        })
})();