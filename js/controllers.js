;(function () {
    var appCtrl = angular.module('MainApp.controllers', ['tw.directives.clickOutside'])

        .controller('MainCtrl', ['$rootScope', '$scope', '$http', function ($rootScope, $scope, $http) {
            $rootScope.auth = {
                logged: localStorage.getItem('logged') && localStorage.getItem('token') ? localStorage.getItem('logged') : false,
                token: localStorage.getItem('logged') && localStorage.getItem('token') ? localStorage.getItem('token') : '',
                user: localStorage.getItem('user') || false
            };

            $scope.refreshToken = function() {
                $rootScope.auth.token ? $http.defaults.headers.common.Authorization = 'Token '+$rootScope.auth.token :
                    delete $http.defaults.headers.common.Authorization;
            };
            $scope.refreshToken();

            $scope.states = {
                registerForm: false,
                loginForm: false
            };
            $scope.data = {
                regData: {},
                loginData: {}
            };
            $scope.forms = {
                login : {
                    state : true,
                    errorMessage : ''
                },
                reg : {
                    state : true,
                    errorMessage: ''
                }
            }
            $scope.toggleLogin = function () {
                $scope.states.loginForm = !$scope.states.loginForm;
                $scope.states.registerForm = false;
            };

            $scope.toggleReg = function () {
                $scope.states.registerForm = !$scope.states.registerForm;
                $scope.states.loginForm = false;
            };

            $scope.loginClearError = function() {
              !$scope.forms.login.state  && ($scope.forms.login.state = true);
            };

            $scope.regClearError = function() {
                $scope.forms.reg.state = true;
            };

            $scope.regRequest = function () {
                $http({
                    method: 'POST',
                    url: getUrl('api/register/'),
                    data: $scope.data.regData
                }).then(function success(response) {
                    var data = response.data;
                    if (data.success) {
                        $scope.data.regData = {};
                        $scope.forms.reg.state = 'success';
                    } else {
                        $scope.forms.reg.state = false;
                        $scope.forms.reg.errorMessage = data.message;
                    }
                }, function regError(response) {
                    console.log(response);
                });
            };
            $scope.loginRequest = function () {
                $http({
                    method: 'POST',
                    url: getUrl('api/login/'),
                    data: $scope.data.loginData
                }).then(function success(response) {
                    var data = response.data;
                    if (data.success && data.token) {
                        $rootScope.auth = {
                            logged: true,
                            token: data.token,
                            user : $scope.data.loginData.username
                        };
                        localStorage.setItem('logged',true);
                        localStorage.setItem('token',data.token);
                        localStorage.setItem('user',$scope.data.loginData.username);
                        $scope.refreshToken();
                        $scope.data.loginData = {};
                    } else {
                        $scope.forms.login.state = false;
                        $scope.forms.login.errorMessage = data.message;
                    }
                }, function regError(data) {
                    console.log(data.data.message);
                });
            };
            $scope.logout = function () {
                $rootScope.auth = {
                    logged: false,
                    token: '',
                    user: false
                };
                localStorage.setItem('logged',false);
                localStorage.setItem('token','');
                localStorage.setItem('user',false);
                $scope.refreshToken();
            };
        }])

        .directive('productsCatalog', ['$rootScope','$http', 'products', function ($rootScope,$http, products) {
            return {
                restrict: 'E',
                templateUrl: 'templates/products.html',
                link: function ($scope) {
                    $scope.path = getUrl('static/');
                    $scope.comments = [];
                    $scope.openForm = [];
                    $scope.newComments = {};
                    $scope.products = [];
                    $scope.getProducts = function() {
                        products.getProducts()
                            .then(function (response) {
                                response.data.map(function(prod) {
                                    $scope.products[prod.id] = prod;
                                });
                                $scope.products.map(function (product) {
                                        products.getComments(product.id)
                                            .then(function (response) {
                                                product.comments = response.data.reverse();
                                            });
                                        $scope.openForm[product.id] = false;
                                    });
                            }, function (response) {
                                console.log(response.message);
                            });
                    };
                    $scope.getProducts();
                    $scope.newComment = function (id) {
                        products.comment(id,$scope.newComments[id])
                            .then(function(response) {
                                if (response.data.success) {
                                    $scope.newComments[id]['created_by'] = {};
                                    $scope.newComments[id]['created_by'].username = $rootScope.auth.user;
                                    $scope.products[id].comments.unshift($scope.newComments[id]);
                                    $scope.newComments[id] = {};
                                }
                            }, function(response) {
                                var data = response.data;
                                $scope.newCommentError = data.message;
                            });
                    };
                    $scope.toggleComments = function(num) {
                        $scope.openForm[num] = !$scope.openForm[num];
                    }
                    $scope.clearNewCommentError = function() {
                        $scope.newCommentError = '';
                    }
                    $scope.clearNewCommentError();
                }
            }
        }])

})();